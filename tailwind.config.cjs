const config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],

  theme: {
    extend: {
        colors: {}
    },
    fontFamily: {
        Poppins: ["Poppins", "sans-serif"]
    },
    container: {
        center: true,
        padding: "1rem",
        screens: {
            lg: "1124px",
            xl: "1124px",
            "2xl": "1124px"
        }
    }
},


  plugins: [],
};

module.exports = config;
