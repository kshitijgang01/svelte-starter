// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyA_aHrp_1nRJ2GDoirqBt47_w23TmmZ0qc",
  authDomain: "form-bot-356111.firebaseapp.com",
  projectId: "form-bot-356111",
  storageBucket: "form-bot-356111.appspot.com",
  messagingSenderId: "370456946607",
  appId: "1:370456946607:web:48111fa3e53942a171715a",
  measurementId: "G-6GVTFFP8L4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app